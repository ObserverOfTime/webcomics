# Transcript of Pepper&Carrot Episode 06 [uk]

## Notes

Providing transcripts is optional for translators.
The Pepper&Carrot site will work fine without them,
but they help for accessibility, screen readers and language learners.

Read https://www.peppercarrot.com/xx/documentation/062_Transcripts.html
for how they can be generated without retyping them after you are done 
translating or updating the content of the speechbubbles in the SVG(s)
and more information and documentation about them.

## Pages

### P00

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Title|1|False|Епізод 6: Змагання зіллєваріння

### P01

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|О-о-ох, я знову заснула з відчиненим вікном...
Pepper|2|True|... ще й вітер ...
Pepper|3|False|... і чому це я бачу Комону у вікно ?
Pepper|4|False|КОМОНУ!
Pepper|5|False|Змагання зіллєваріння!
Pepper|6|False|Я, мабуть... мабуть, я випадково заснула*!
Pepper|9|True|... та однак ...
Pepper|10|False|Де я ?!?
Bird|12|False|кря?
Note|7|False|* див. епізод 4 : Напад геніальності|nowhitespace

### P02

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|!!!
Pepper|2|False|Морквич! Як мило, що ти піклуєшся про те, щоб доставити мене на змагання!
Pepper|3|False|Фан-тас-ти-ка !
Pepper|4|True|Ти навіть здогадався взяти зелля, мій одяг і капелюх...
Pepper|5|False|... поглянемо, яке зілля ти взяв ...

### P03

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|False|ЩО?!!
Mayor of Komona|3|False|На правах мера міста Комона, я оголошую змагання зіллєваріння... Відкритим!
Mayor of Komona|4|False|Наше місто раде зустріти на першому такому змаганні аж чотирьох відьом!
Mayor of Komona|5|True|Отож, нумо їм
Mayor of Komona|6|True|гучно
Writing|2|False|Змагання Зіллєварів Комони
Mayor of Komona|7|False|поаплодуємо !

### P04

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Audience|29|False|Плеск
Mayor of Komona|1|True|Це честь бачити тут приголомшливу майстриню, що прибула прямо з Техносоюзу -
Mayor of Komona|3|True|... пам’ятаймо й про нашу власну відьму Комони -
Mayor of Komona|5|True|... Наша третя учасниця - гостя з країни призахідного місяця,
Mayor of Komona|7|True|... і нарешті, наша остання учасниця з лісу Білячого Кутку,
Mayor of Komona|2|False|Коріандру !
Mayor of Komona|4|False|Сафрон !
Mayor of Komona|6|False|Сітімі !
Mayor of Komona|8|False|Пеппер !
Mayor of Komona|9|True|Починаймо гру!
Mayor of Komona|10|False|Голосувати будемо оплесками
Mayor of Komona|11|False|Першою буде демонстрація Коріандри!
Coriander|13|False|... Смерть більш не страшна завдяки моєму ...
Coriander|14|True|... Зіллю
Coriander|15|False|ЗОМБІФІКАЦІЇ !
Audience|16|True|Плеск
Audience|17|True|Плеск
Audience|18|True|Плеск
Audience|19|True|Плеск
Audience|20|True|Плеск
Audience|21|True|Плеск
Audience|22|True|Плеск
Audience|23|True|Плеск
Audience|24|True|Плеск
Audience|25|True|Плеск
Audience|26|True|Плеск
Audience|27|True|Плеск
Audience|28|True|Плеск
Coriander|12|False|Пані та панове...

### P05

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|ДИВОВИЖНО !
Audience|3|True|Плеск
Audience|4|True|Плеск
Audience|5|True|Плеск
Audience|6|True|Плеск
Audience|7|True|Плеск
Audience|8|True|Плеск
Audience|9|True|Плеск
Audience|10|True|Плеск
Audience|11|True|Плеск
Audience|12|True|Плеск
Audience|13|True|Плеск
Audience|14|True|Плеск
Audience|15|True|Плеск
Audience|16|False|Плеск
Saffron|18|True|Настав час оцінити
Saffron|17|True|... жителі комони, будьте так ласкаві, прибережіть свої оплески !
Saffron|22|False|... та змусить їх заздрити!
Saffron|19|True|МОЄ
Saffron|25|False|РОЗКІШНОСТІ !
Saffron|24|True|... Зілля
Saffron|23|False|... і все це можливо за допомогою лише однієї краплі мого ...
Audience|26|True|Плеск
Audience|27|True|Плеск
Audience|28|True|Плеск
Audience|29|True|Плеск
Audience|30|True|Плеск
Audience|31|True|Плеск
Audience|32|True|Плеск
Audience|33|True|Плеск
Audience|34|True|Плеск
Audience|35|True|Плеск
Audience|36|True|Плеск
Audience|37|True|Плеск
Audience|38|True|Плеск
Audience|39|True|Плеск
Audience|40|False|Плеск
Mayor of Komona|42|False|Це зілля всю Комону може зробити багатою!
Mayor of Komona|41|True|Фантастично! Неймовірно !
Audience|44|True|Плеск
Audience|45|True|Плеск
Audience|46|True|Плеск
Audience|47|True|Плеск
Audience|48|True|Плеск
Audience|49|True|Плеск
Audience|50|True|Плеск
Audience|51|True|Плеск
Audience|52|True|Плеск
Audience|53|True|Плеск
Audience|54|True|Плеск
Audience|55|True|Плеск
Audience|56|True|Плеск
Audience|57|True|Плеск
Audience|58|True|Плеск
Audience|59|True|Плеск
Audience|60|False|Плеск
Mayor of Komona|2|False|Цим чу-до-дій-ним зіллям Коріандра кидає виклик самій смерті!
Saffron|21|True|Зілля, якого ви всі по-справжньому чекали: зілля, що вразить ваших сусідів ...
Mayor of Komona|43|False|Ваші оплески не можуть помилятися. Коріандру вже викреслено зі списку!
Saffron|20|False|зілля!

### P06

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Mayor of Komona|1|False|Сітімі буде складно перевершити останню демонстрацію !
Shichimi|4|True|НІ!
Shichimi|5|True|Не треба, це занадто небезпечно!
Shichimi|6|False|Вибачте !
Mayor of Komona|3|False|... нумо, Сітімі, тебе всі чекають!
Mayor of Komona|7|False|Пані та панове, здається, що Сітімі позбавлено...
Saffron|8|False|Віддай !
Saffron|9|False|... і досить прикидатися сором’язливою, ти псуєш все шоу
Saffron|10|False|Нема різниці, що там твоє зілля робить, всім вже й без нього зрозуміло, що я виграла змагання ...
Shichimi|11|False|!!!
Sound|12|False|вжжжЖЖУХ!|nowhitespace
Shichimi|15|False|ВЕЛИЧЕЗНИХ ЧУДОВИСЬК !
Shichimi|2|False|Я... Я не знала, що потрібно ще й демонструвати...
Shichimi|13|True|ОБЕРЕЖНО!!!
Shichimi|14|True|Це зілля

### P07

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Bird|1|False|КО-КО-Кооо о оо|nowhitespace
Sound|2|False|БАМ!
Pepper|3|True|... ха, клас !..
Pepper|5|False|... у всякому разі, моє зілля зірве трохи смішинок, тому що ....
Pepper|4|False|і що, тепер моя черга ?
Mayor of Komona|6|True|Біжи, дурепа!
Mayor of Komona|7|False|Змагання закінчено ! ...рятуйся!

### P08

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|2|False|... як завжди, коли наша черга - усі йдуть ...
Pepper|1|True|оце так ...
Pepper|4|True|Зате в мене хоча б є ідея, як пристосувати твоє "зілля", Морквич
Pepper|5|False|...давай тут все владнаємо, і вирушимо додому!
Pepper|7|True|Ти,
Pepper|8|False|модне переросле зомбопташеня !
Pepper|10|False|Хочеш спробувати останнє зілля? ...
Pepper|11|False|... не дуже, га ?
Pepper|6|False|ГЕЙ !
Sound|9|False|Хрясь !|nowhitespace

### P09

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Pepper|1|True|Отож, читай з папірця, уважно читай ...
Pepper|2|False|... Я довго думати не буду, я його відразу на тебе виллю, якщо ти не заберешся з Комони негайно!
Mayor of Komona|3|True|За те, що врятувала наше місто, коли воно було в небезпеці,
Mayor of Komona|4|False|ми нагороджуємо першим місцем Пеппер за її Зілля... Зілля чого... ??!!
Pepper|7|False|... ха... взагалі, це зовсім не зілля; це склянка с аналізами мого кота з останнього відвідування ветеринара!
Pepper|6|True|... Ха-ха! та от ...
Pepper|8|False|... так що, демонстрації не буде ?...
Narrator|9|False|Епізод 6 : Змагання зілляваріння
Narrator|10|False|КІНЕЦЬ
Writing|5|False|50,000 Ko
Credits|11|False|Березень 2015 - Сюжет та графіка - David Revoy. Переклад - uncle Night

### P10

Name|Position|Concatenate|Text|Whitespace (Optional)
----|--------|-----------|----|---------------------
Credits|1|False|Pepper&Carrot - цілком безкоштовний, його матеріали відкриті, комікс спонсується завдяки допомозі його помічників. 245 помічників зробили внесок до цього епізоду :
Credits|4|False|https://www.patreon.com/davidrevoy
Credits|3|False|Ви теж можете стати спонсором Pepper&Carrot на наступний епізод:
Credits|7|False|Інструменти : цей епізод було створено за допомогою 100% безкоштовного програмного забезпечення Krita на Linux Mint
Credits|6|False|Відкриті джерела : шаровані файли джерел високої роздільної здатності для друку, з шрифтами, можна завантажувати, продавати, змінювати, перекладати, і т.д...
Credits|5|False|Ліцензовано : Creative Commons Attribution на ім’я 'David Revoy' можна змінювати, пересилати, продавати, і т.і...
Credits|2|False|Алексей ★ Глеб Бузало ★ 獨孤欣 & 獨弧悦 ★ Addison Lewis ★ A Distinguished Robot ★ Adrian Lord ★ Ahmad Ali ★ Aina Reich ★ Alandran ★ Alan Hardman ★ Albert Westra ★ Alcide ★ Alejandro Flores Prieto ★ Alex ★ Alexander Bülow Tomassen ★ Alexander Sopicki ★ Alexandra Jordan ★ Alex Lusco ★ Alex Silver ★ Alex Vandiver ★ Alfredo ★ Ali Poulton (Aunty Pol) ★ Allan Zieser ★ Andreas Rieger ★ Andrej Kwadrin ★ Andrew ★ Andrew Godfrey ★ Andrey Alekseenko ★ Angela K ★ Anna Orlova ★ Antan Karmola ★ Anthony Edlin ★ Antonio Mendoza ★ Ardash Crowfoot ★ Arjun Chennu ★ Arne Brix ★ Aslak Kjølås-Sæverud ★ Axel Bordelon ★ Axel Philipsenburg ★ Barbix ★ Ben Evans ★ Bernd ★ Betsy Luntao ★ Boonsak Watanavisit ★ Boris Fauret ★ Boudewijn Rempt ★ BoxBoy ★ Brett Smith ★ Brian Behnke ★ Brian Smith ★ Bryan Butler ★ Bui Dang Hai Trieu ★ Carlos Levischi ★ Charlotte Lacombe-bar ★ Chris Radcliff ★ Chris Sakkas ★ Christian Gruenwaldner ★ Christophe Carré ★ Christopher Bates ★ Clara Dexter ★ codl ★ Colby Driedger ★ Conway Scott Smith ★ Cuthbert Williams ★ Cyrille Largillier ★ Cyril Paciullo ★ Damien ★ Daniel ★ Daniel Björkman ★ Danny Grimm ★ David Tang ★ DiCola Jamn ★ Dmitry ★ Donald Hayward ★ Duke ★ Eitan Goldshtrom ★ Enrico Billich ★ Epsilon ★ Eric Schulz ★ Faolan Grady ★ Francois Schnell ★ Garret Patterson ★ Ginny Hendricks ★ GreenAngel5 ★ Grigory Petrov ★ Guillaume ★ Guillaume Ballue ★ Gustav Strömbom ★ Guy Davis ★ Happy Mimic ★ Helmar Suschka ★ Henning Döscher ★ Ilyas ★ Irina Rempt ★ Ivan Korotkov ★ James Frazier ★ Janusz ★ Jared Tritsch ★ JDB ★ Jean-Baptiste Hebbrecht ★ Jean-Gabriel LOQUET ★ Jeffrey Schneider ★ Jessey Wright ★ Jim ★ Jim Street ★ Jiska ★ Joachim Schiele ★ JoÃ£o Luiz Machado Junior ★ Joern Konopka ★ joe rutledge ★ Johanne Thomson ★ John ★ John Urquhart Ferguson ★ Jónatan Nilsson ★ Jonathan Leroy ★ Jonathan Ringstad ★ Jon Brake ★ Jorge Bernal ★ Joseph Bowman ★ Julien Duroure ★ Justus Kat ★ Kai-Ting (Danil) Ko ★ Kasper Hansen ★ Kate ★ Kathryn Wuerstl ★ Ken Mingyuan Xia ★ Kingsquee ★ Kroet ★ Levi Kornelsen ★ Liselle ★ Lorentz Grip ★ L S ★ Luc Stepniewski ★ Luke Hochrein ★ MacCoy ★ Magnus Kronnäs ★ Manuel ★ Marc & Rick ★ Marcus ★ Martin Owens ★ Mary Brownlee ★ Masked Admirer ★ Mathias Stærk ★ Mefflin Ross Bullis-bates ★ Michael ★ Michael Gill ★ Michael Pureka ★ Michelle Pereira Garcia ★ Mike Mosher ★ Miroslav ★ Muzyka Dmytro ★ Nataya Castillo ★ Nazhif ★ Nicholas DeLateur ★ Nicholas Terranova ★ Nicki Aya ★ Nicola Angel ★ Nicolae Berbece ★ Nicole Heersema ★ Nielas Sinclair ★ NinjaKnight Comics ★ Noble Hays ★ Noelia Calles Marcos ★ Nora Czaykowski ★ Nyx ★ Olivier Amrein ★ Olivier Brun ★ Omar Willey ★ Oscar Moreno ★ Öykü Su Gürler ★ Ozone S. ★ Pablo Lopez Soriano ★ Pat David ★ Patrick Gamblin ★ Paul ★ Pavel Semenov ★ Pet0r ★ Peter ★ Peter Moonen ★ Petr Vlašic ★ Philippe Jean Edward Bateman ★ Pierre Geier ★ Pierre Vuillemin ★ Pranab Shenoy ★ Pyves & Ran ★ Raghavendra Kamath ★ Rajul Gupta ★ Reorx Meng ★ Ret Samys ★ Rictic ★ RJ van der Weide ★ Roberto Zaghis ★ Roman ★ Rumiko Hoshino ★ Rustin Simons ★ Sally Bridgewater ★ Sami T ★ Samuel Mitson ★ Scott Petrovic ★ Sean Adams ★ Shadefalcon ★ ShadowMist ★ Shafak ★ Shawn Meyer ★ Simon Forster ★ Simon Isenberg ★ Sonny W. ★ Soriac ★ Stanislav Vodetskyi ★ Stephanie Cheshire ★ Stephen Bates ★ Stephen Smoogen ★ Steven Bennett ★ Stuart Dickson ★ Surt ★ TamaskanLEM ★ Tar8156 ★ Terry Hancock ★ TheFaico ★ Thomas Citharel ★ Thomas Courbon ★ Thomas Schwery ★ Tim Burbank ★ Tim J. ★ Tomas Hajek ★ Tom Demian ★ Tom Savage ★ Tracey Reuben ★ Travis Humble ★ Tree ★ Tyson Tan ★ Urm ★ Victoria ★ Victoria White ★ Vladislav Kurdyukov ★ Vlad Tomash ★ Westen Curry ★ Witt N. Vest ★ Xavier Claude ★ Yalyn Vinkindo ★ Yaroslav ★ Zeni Pong ★ Źmicier Kušnaroŭ
